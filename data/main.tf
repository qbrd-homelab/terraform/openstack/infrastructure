module "flavors" { source = "./flavors" }
module "images" { source = "./images" }

output "flavors" { value = module.flavors }
output "images" { value = module.images }
