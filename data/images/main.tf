data "openstack_images_image_v2" "alpine" {
  name        = "alpine-virt-3-10-2-x86_64-20191003195416"
  most_recent = true
}

output "alpine" {
  value = data.openstack_images_image_v2.alpine
}
