variable "external_network_name" {
  default = "public"
}

variable "dns_domain" {
  default = "qbrd.cc."
}

variable "dns_nameservers" {
  default = ["192.168.0.2"]
}
