variable "image" {
  # type        = object(image)
  description = "ID of Image to clone"
}

variable "flavor" {
  # type        = object(flavor)
  description = "ID of flavor for Image"
}

variable "key_pair" {
  type        = string
  description = "Name of Keypair to authenticate"
}

variable "security_groups" {
  type        = list(string)
  description = "List of security groups to apply to instance"
}

variable "network" {
  # type        = object(network)
  description = "The ID of network to attach to instance"
}

module "prod" {
  source          = "git::https://gitlab.com/qbrd-homelab/terraform/openstack/modules.git//instance_basic"
  vm_name         = "consul-prod"
  vm_count        = 5
  image_id        = var.image.id
  flavor_id       = var.flavor.id
  key_pair        = var.key_pair
  security_groups = var.security_groups
  network_id      = var.network.id
}

output "prod" {
  value = module.prod
}
