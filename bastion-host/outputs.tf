output vms {
  value = openstack_compute_instance_v2.bastion.*
}

output ips {
  value = openstack_compute_floatingip_associate_v2.floating_ip_to_basic_instance
}

output manual_map {
  value = {
    dev    = openstack_compute_floatingip_associate_v2.floating_ip_to_basic_instance["dev"].floating_ip
    test   = openstack_compute_floatingip_associate_v2.floating_ip_to_basic_instance["test"].floating_ip
    prod01 = openstack_compute_floatingip_associate_v2.floating_ip_to_basic_instance["prod01"].floating_ip
    prod02 = openstack_compute_floatingip_associate_v2.floating_ip_to_basic_instance["prod02"].floating_ip
  }
}
