variable data {
  description = "data object with flavor and image info"
}

variable networks {
  description = "networks object"
}

variable key_pair {
  type        = string
  description = "name of key-pair"
}
