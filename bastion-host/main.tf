resource openstack_compute_instance_v2 bastion {
  for_each        = var.networks.private_networks
  name            = format("%s-%s-%02d", "bastion", each.key, 1)
  image_id        = var.data.images.alpine.id
  flavor_id       = var.data.flavors.m1-tiny.id
  key_pair        = var.key_pair
  security_groups = [var.networks.security-groups.secgroup_allow_all.name]
  network {
    uuid = each.value.network.id
  }
}

resource openstack_networking_floatingip_v2 floating_ip {
  for_each    = openstack_compute_instance_v2.bastion
  pool        = var.networks.external-network.network.name
  description = "${each.value.name} floating IP"
}

# floating ip is associated with vm
resource openstack_compute_floatingip_associate_v2 floating_ip_to_basic_instance {
  for_each    = openstack_compute_instance_v2.bastion
  floating_ip = openstack_networking_floatingip_v2.floating_ip[each.key].address
  instance_id = each.value.id
}
