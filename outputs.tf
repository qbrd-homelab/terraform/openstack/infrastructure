output "zones" {
  value     = data.openstack_compute_availability_zones_v2.zones
  sensitive = true
}

output "data" {
  value     = module.data
  sensitive = true
}

output "networks" {
  value     = module.networks
  sensitive = true
}

output "bastion-host" {
  value     = module.bastion-host
  sensitive = true
}

output "consul" {
  value     = module.consul
  sensitive = true
}

output bastion-host-ips {
  value = module.bastion-host.manual_map
}
