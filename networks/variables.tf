variable "dns_nameservers" {
  type        = list
  description = "Array of nameservers"
  default     = ["192.168.0.2"]
}

variable "external_network_name" {
  type        = string
  description = "Name of external network"
  default     = "public"
}
