variable "external_network_id" {
  type = "string"
}

variable "dns_nameservers" {
  type = list(string)
}

module "dev" {
  source              = "git::https://gitlab.com/qbrd-homelab/terraform/openstack/modules.git//network"
  external_network_id = var.external_network_id
  dns_nameservers     = var.dns_nameservers
  name                = "dev"
  cidr                = "10.10.0.0/24"
}

module "test" {
  source              = "git::https://gitlab.com/qbrd-homelab/terraform/openstack/modules.git//network"
  external_network_id = var.external_network_id
  dns_nameservers     = var.dns_nameservers
  name                = "test"
  cidr                = "10.10.100.0/24"
}

module "prod01" {
  source              = "git::https://gitlab.com/qbrd-homelab/terraform/openstack/modules.git//network"
  external_network_id = var.external_network_id
  dns_nameservers     = var.dns_nameservers
  name                = "prod01"
  cidr                = "192.168.100.0/24"
}

module "prod02" {
  source              = "git::https://gitlab.com/qbrd-homelab/terraform/openstack/modules.git//network"
  external_network_id = var.external_network_id
  dns_nameservers     = var.dns_nameservers
  name                = "prod02"
  cidr                = "192.168.200.0/24"
}
