resource "openstack_networking_network_v2" "external" {
  name           = var.external_network_name
  description    = var.external_network_description
  admin_state_up = "true"
  external       = "true"
  segments {
    physical_network = "br-ex"
    network_type = "flat"
  }
}

resource "openstack_networking_subnet_v2" "external-subnet" {
  name       = "${var.external_network_name}-subnet"
  network_id = "${openstack_networking_network_v2.external.id}"
  cidr       = var.external_network_cidr
  ip_version = var.external_network_ip_version
}
