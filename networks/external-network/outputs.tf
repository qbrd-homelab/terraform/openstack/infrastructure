output "network" {
  value = openstack_networking_network_v2.external
}

output "subnet" {
  value = openstack_networking_subnet_v2.external-subnet
}
