variable "external_network_name" {
  description = "Name of the external network"
  type        = string
}

variable "external_network_description" {
  description = "Description of the external network"
  type        = string
  default     = "External network for Floating IP assignment"
}

variable "external_network_cidr" {
  description = "CIDR of external network, e.g.: 10.10.10.0/24"
  type        = string
}
  
variable "external_network_ip_version" {
  description = "IP Version of external Network,  4 or 6"
  type        = number
  default     = 4
}
