output security-groups {
  value = module.security-groups
}

output external-network {
  value = module.external-network
}

output private {
  value = module.private
}

output private_networks {
  value = {
    dev    = module.private.dev
    test   = module.private.test
    prod01 = module.private.prod01
    prod02 = module.private.prod02
  }
}
