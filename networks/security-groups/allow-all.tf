variable "secgroup_allow_all_name" {
  default     = "allow-all"
  description = "Name of security group rule"
}

variable "secgroup_allow_all_description" {
  default     = "Debug security group to allow all ICMP, UDP, and TCP traffic"
  description = "Explanation as to why we're basically turning off the firewall"
}

resource "openstack_networking_secgroup_v2" "secgroup_allow_all" {
  name        = var.secgroup_allow_all_name
  description = var.secgroup_allow_all_description
}

resource "openstack_networking_secgroup_rule_v2" "tcp_in_v4" {
  depends_on        = [openstack_networking_secgroup_v2.secgroup_allow_all]
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_allow_all.id
}

resource "openstack_networking_secgroup_rule_v2" "tcp_out_v4" {
  depends_on        = [openstack_networking_secgroup_v2.secgroup_allow_all]
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_allow_all.id
}

resource "openstack_networking_secgroup_rule_v2" "udp_in_v4" {
  depends_on        = [openstack_networking_secgroup_v2.secgroup_allow_all]
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_allow_all.id
}

resource "openstack_networking_secgroup_rule_v2" "udp_out_v4" {
  depends_on        = [openstack_networking_secgroup_v2.secgroup_allow_all]
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_allow_all.id
}

resource "openstack_networking_secgroup_rule_v2" "icmp_in_v4" {
  depends_on        = [openstack_networking_secgroup_v2.secgroup_allow_all]
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_allow_all.id
}

resource "openstack_networking_secgroup_rule_v2" "icmp_out_v4" {
  depends_on        = [openstack_networking_secgroup_v2.secgroup_allow_all]
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_allow_all.id
}
