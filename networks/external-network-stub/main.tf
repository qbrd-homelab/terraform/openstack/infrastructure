variable "external_network_name" {
  type        = string
  description = "name of external network"
  default     = "public"
}

data "openstack_networking_network_v2" "network" {
  name = var.external_network_name
}

output "network" {
  value = data.openstack_networking_network_v2.network
}
