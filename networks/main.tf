module "security-groups" { source = "./security-groups" }

module "external-network" {
  source                = "./external-network-stub"
  external_network_name = var.external_network_name
  #   external_network_cidr = "10.0.0.0/24"
}

module "private" {
  source              = "./private"
  external_network_id = module.external-network.network.id
  # external_network_id = data.openstack_networking_network_v2.external_network.id
  dns_nameservers = var.dns_nameservers
}
