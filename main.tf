provider "openstack" {}

module "data" { source = "./data" }
module "networks" { source = "./networks" }

module "bastion-host" {
  source   = "./bastion-host"
  data     = module.data
  networks = module.networks
  key_pair = "admin"
}

module "consul" {
  source          = "./consul"
  image           = module.data.images.alpine
  flavor          = module.data.flavors.m1-tiny
  network         = module.networks.private.prod01.network
  security_groups = [module.networks.security-groups.secgroup_allow_all.name]
  key_pair        = "admin"
  # external_network_id = module.networks.external-network.network.id
}
